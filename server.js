'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

var mysql = require('mysql');
var os = require("os");
var hostname = os.hostname();
console.log("Current hstname:", hostname);

var con = mysql.createConnection({
  host: process.env.HOST_ID,
  user: "root",
  password: process.env.PASS_ID
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});


// Perform a query
var row;
var query = 'select @@hostname as host';
con.query(query, function(err, rows, fields) {
    if(err){
        console.log("An error ocurred performing the query.");
        return;
    }
    Object.keys(fields).forEach(function(key) {      
      row = rows[key].host;
      console.log("Query succesfully executed:", row);
    });
});

// Close the connection
con.end(function(){
    // The connection has been closed
});

// App
const app = express();
app.get('/', (req, res) => {
  res.send({Mysql_pod_name: row, this_host: hostname});
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
console.log(``)
